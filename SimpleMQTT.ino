// --------------------
// Control an RGB LED over MQTT
// Designed for use with a WeMos D1 R1 board
// Use with YouTube video: 
// --------------------

// Define your variables here
// LED Pins
const int RED_PIN = D11;
const int GREEN_PIN = D12;
const int BLUE_PIN = D13;

// Your Wi-Fi Credentials
const char WIFI_USERNAME[50] = "xxxxx";
const char WIFI_PASSWORD[50] = "xxxxx";

// Your MQTT Details
const char MQTT_URL[50] = "xxxxx";
const char MQTT_USERNAME[50] = "xxxxx";
const char MQTT_TOPIC_RED[50] = "xxxxx";
const char MQTT_TOPIC_GREEN[50] = "xxxxx";
const char MQTT_TOPIC_BLUE[50] = "xxxxx";
const char MQTT_TOPIC_RESET[50] = "xxxxx";


#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <PubSubClient.h>

void WiFi_Connect();
void MQTT_Connect();
void callback(char*, byte*, unsigned int);

WiFiClient wificlient;
PubSubClient mqtt(wificlient);

void setup() {
  Serial.begin(9600);
  Serial.println("Starting");

  pinMode(RED_PIN, OUTPUT); // Red
  pinMode(GREEN_PIN, OUTPUT); // Green
  pinMode(BLUE_PIN, OUTPUT); // Blue

  pinMode(D8, OUTPUT);  // "Power source"
  digitalWrite(D8, HIGH);

  // Off all colors first
  // Set to HIGH as we are using these pins as sinks
  pinMode(RED_PIN, HIGH);
  pinMode(GREEN_PIN, HIGH);
  pinMode(BLUE_PIN, HIGH);

  WiFi_Connect();
  MQTT_Connect();

  mqtt.subscribe(MQTT_TOPIC_RED);
  mqtt.subscribe(MQTT_TOPIC_GREEN);
  mqtt.subscribe(MQTT_TOPIC_BLUE);
  mqtt.subscribe(MQTT_TOPIC_RESET);
}

void loop() {
  mqtt.loop();
}

void WiFi_Connect() {
  WiFi.disconnect();
  WiFi.begin(WIFI_USERNAME, WIFI_PASSWORD);
  Serial.print("Connecting Wifi.");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println(" Wifi Connected");
}

void MQTT_Connect() {
  mqtt.setServer(MQTT_URL, 1883);
  mqtt.setCallback(callback);

  Serial.print("Connecting MQTT.");
  while (!mqtt.connected()) {
    mqtt.connect(MQTT_USERNAME);
    delay(500);
    Serial.print(".");
  }

  Serial.println(" MQTT Connected");
}

void callback(char* topic, byte* payload, unsigned int length) {
  char myStr[20];
  strncpy(myStr, (char*)payload, length);
  int value = String(myStr).toInt();

  Serial.print("[");
  Serial.print(topic);
  Serial.print("] ");
  Serial.println(value);

  if (strcmp(topic, MQTT_TOPIC_RED) == 0) {
    analogWrite(RED_PIN, 1023 - value);
  }
  else if (strcmp(topic, MQTT_TOPIC_GREEN) == 0) {
    analogWrite(GREEN_PIN, 1023 - value);
  }
  else if (strcmp(topic, MQTT_TOPIC_BLUE) == 0) {
    analogWrite(BLUE_PIN, 1023 - value);
  }
  else if (strcmp(topic, MQTT_TOPIC_RESET) == 0) {
    analogWrite(RED_PIN, 1023);
    analogWrite(GREEN_PIN, 1023);
    analogWrite(BLUE_PIN, 1023);
  }
}
